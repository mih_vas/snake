unit SnakeUnit;

interface

uses
  Windows, Messages, Graphics, ExtCtrls, Classes, Forms, SysUtils, Dialogs;

const
  WM_IMOVING = WM_USER + 100;                    //��������� � ���� ������ - ������������ ����� ������� ����� ����

type
  //��� ����
  TGameType = (gtOnce,                           //������ ���� �������
               gtOnceWithComputer,               //������ ������� � ��������� �� ����� ����������
               gtTwiceOnComputer,                //������ ��� �������� �� ����� ����������
               gtTwiceOnNetwork,                 //������ ��� �������� �� ����
               gtNone);                          //��� ����

  //��� ���������� � ������������
  TOrientation = (oNoth,                         //�������� �����
                  oSouth,                        //�������� ����
                  oWest,                         //�������� �����
                  oEast);                        //�������� ������

  //��� ������� ������
  TStatus = (sWinner,                            //����������
             sLoser,                             //�����������
             sNone);                             //��� ����������

  //����� ������
  TSnakeBits = class(TObject)
   {��� ��� ��� �������� �� ������ ������ ���������� ������ ����� ������ ������,
    �� ��� ���� � ������ �������� � ������ private, ����������� ������ ���������
    � ������ public}
   private
    Color: TColor;                               //����
    PlaceMent: TPoint;                           //��������������
    Width, Height: Integer;                      //������ � ������
    dX, dY: Integer;                             //�������� �������� ���� ������������ �����
    Orientation: TOrientation;                   //����������
    Canvas: TCanvas;                             //��� ����� ��������
    procedure Show(Transparent: Boolean);        //����������� �����
    function Replace(Orient: TOrientation): TPoint; //����������� ����� (���������� ����� �����������������)
   public
    constructor Create(pl: TPoint; w, h: Integer; C: TCanvas; ddX, ddY: Integer); //�����������
  end;

  //������
  TSnake = class(TObject)
   {��� ���� ������� "������" �������� � ������ private, ����������� � ����������
    ������ ��������� � ������ public, ��� �� ������� ��������: Orientation,
    ������� ����� �������� �������� � �������� � ���� FOrientation; Speed - ��������
    �������� �� ���� Fspeed, ������� � ������� ��������� GetSpeed (��� ��������� �����
    �������� Timer.Interval); Health - �������� �������� �� ���� FHealth, ������������
    ����� ����� SetHealh (�������� �� ��������); Alive - ������� ����� � ������, �������/��������
    ����� �� ���� FALive; CountBits - �������� �������� �� FCountBits, ������� ����� �����
    SetCountBits (�������� ���������). ��������� ������ ���������� ������������,
    � �������������, ������ ��� ��� ������ ����� ������ �� �������. ������, ������,
    ������������ � ���������, �� ����� ���� ������������, ������ �����������}
   private
    PlaceMent: TPoint;                           //��������������
    FCountBits: Byte;                            //���������� ������� ������
    FOrientation: TOrientation;                  //����������
    Timer: TTimer;                               //������ ��� �������� ������
    FSpeed: Integer;                             //�������� �����������
    FHealth: Integer;                            //��������
    SizeCellX, SizeCellY: Byte;                  //������ ����� �� ����������� � ���������
    Canvas: TCanvas;                             //��� ����� ��������
    dX, dY: Integer;                             //�������� �������� ���� ������������ �����
    SnakeBits: array [1..100] of TSnakeBits;     //������ ������
    Tag: Integer;                                //����� ������ (����, ���� ����� ����� �����)
    FALive: Boolean;                             //�������, ��� ������ ����
    procedure Show;                              //����������� ������
    procedure Move;                              //�������� ������
    procedure INeedMove(Sender: TObject);        //������� ������� �� �������� ������
    procedure RemoveBits(Index: Byte); dynamic;  //�������� ����� ������
    procedure AddBits(Index: Byte); dynamic;     //���������� ����� ������
    procedure SetSpeed(ANewValue: Integer); virtual;     //��� ������ ������ �������� �������� "��������"
    procedure SetHealth(ANewValue: Integer); virtual;    //��� ������ ������ �������� �������� "��������"
    procedure SetCountBits(ANewValue: Byte); virtual;    //��� ������ ������ �������� �������� "�����"
   public
    constructor Create(pl: TPoint; scX, scY: Byte; cb: Byte; C: TCanvas; ddX, ddY: Integer; t: Integer); //�����������
    destructor Destroy; override;                //����������
    property Orientation: TOrientation read FOrientation write FOrientation;    //�������� "����������"
    property ALive: Boolean read FALive write FALive;    //�������� "�����"
    property Speed: Integer read FSpeed write SetSpeed;    //�������� "��������"
    property Health: Integer read FHealth write SetHealth;    //�������� "��������"
    property CountBits: Byte read FCountBits write SetCountBits;    //�������� "�����"
  end;

  //�����-������
  TBonus = class(TObject)
   {��� ���� � private, ����� ������������, �.�. Action ����� ���������������
    � ������-������� � ����� ������, �� ��� ����� ������� � protected, �� ����������
    � ���� � ����� ������ ������}
   private
    Tag: Byte;                                   //����������������� ����� ������
    PlaceMent: TPoint;                           //��������������
    dX, dY: Integer;                             //�������� �������� ���� ������������ �����
    Canvas: TCanvas;                             //��� ����� ��������
    Color: TColor;                               //���� ������ ��� ���������
    SizeCellX, SizeCellY: Byte;                  //������ ����� �� ����������� � ���������
    Value: Integer;                              //�������� ������
    Ball: Integer;                               //���� ������
    procedure Show(Transparent: Boolean);        //����������� ������
   protected
    function Action(Snake: TSnake): Integer; virtual; abstract; //�������� ��� ������������ (����������� ����� - ������� �� ����������, �� ������������ � ���������������� � ��������)
   public
    constructor Create(pl: TPoint; ddx,ddy: Integer; c: TCanvas; scx, scy: Byte; t: Byte; Up: Boolean = True); //�����������
  end;

  //�����-��������
  TBonusSpeed = class(TBonus)
   {����� �� ����� ��������� ������� � �����}
   private
   public
    constructor Create(pl: TPoint; ddx,ddy: Integer; c: TCanvas; scx, scy: Byte; t: Byte; Up: Boolean = True); //�����������
    function Action(Snake: TSnake): Integer; override; //�������� ��� ������������
  end;

  //�����-��������
  TBonusHealth = class(TBonus)
   {����� �� ����� ��������� ������� � �����}
   private
   public
    constructor Create(pl: TPoint; ddx,ddy: Integer; c: TCanvas; scx, scy: Byte; t: Byte; Up: Boolean = True); //�����������
    function Action(Snake: TSnake): Integer; override; //�������� ��� ������������
  end;

  //�����-�����
  TBonusLength = class(TBonus)
   {����� �� ����� ��������� ������� � �����}
   private
   public
    constructor Create(pl: TPoint; ddx,ddy: Integer; c: TCanvas; scx, scy: Byte; t: Byte; Up: Boolean = True); //�����������
    function Action(Snake: TSnake): Integer; override; //�������� ��� ������������
  end;

  //�����-������
  TGamer = class(TObject)
   {������ ���������� Name � Score}
   private
    FName: String;                               //��� ������
    FScore: Integer;                             //����
    Status: TStatus;                             //������ ������
   public
    constructor Create;                          //�����������
    property Name: String read FName write FName;//�������� "��� ������"
    property Score: Integer read FScore write FScore;//�������� "����"
  end;

  //�����-�������
  THumanGamer = class(TGamer)
   private
   public
  end;

  //�����-���������
  TComputerGamer = class(TGamer)
   private
   public
  end;

  //������� ����
  TGameField = class(TObject)
   {��� ��� ���� ����� �������� ��������, �� ���� � ��� ���������������� ��� ���������
    � �� ������ ��� �� ������, �� ���� ����� ���� ����� �������� � �����, �� �������
    ����������� ��� ���� ������ ���� ��������������� ����������, � ������ �������� �
    ������ public ��� published}
   private
    CountCellX, CountCellY: Byte;                //���������� ����� ����� �� ����������� � ���������
    Canvas: TCanvas;                             //��� ����� ��������
    FPlaceMent: TPoint;                          //����������������� ����
    procedure Collision(Who, Reason: Byte); dynamic; //��������� ����� ������������ �� ����
    procedure RemoveBonus(Index: Byte); dynamic; //�������� ���������� ������
    function AddBonus(Index: Byte): TPoint; dynamic; //���������� ������
    function GetPlaceMent: TPoint; virtual;      //��� ������ �������� �������� "��������������"
    procedure SetPlaceMent(ANewValue: TPoint); virtual;  //��� ������ �������� �������� "��������������"
   public
    Width, Height: Integer;                      //������ � ������ ����
    SizeCellX, SizeCellY: Byte;                  //������ ����� �� ����������� � ���������
    Snake: array [1..2] of TSnake;               //������ (�� ���������� �������)
    CountSnake: Byte;                            //���������� �����
    GameNow: Boolean;                            //������� ������� ����
    HumanGamer: THumanGamer;                     //�����-�������
    HumanGamer2: THumanGamer;                    //������ �����-�������
    ComputerGamer: TComputerGamer;               //�����-���������
    Bonus: array [1..10] of TBonus;              //������ (������������ �� ���� ����� ���� 10 �������)
    GameType: TGameType;                         //��� ����
    Map: array [0..101, 0..101] of Integer;      {�������� ����� ����: -1 - ������ �����,
                                                                       0 - ��������� ������������,
                                                                       1 - ������ ������,
                                                                       2 - ������ ������,
                                                                       3 - ����� ���������� ��������,
                                                                       4 - ����� ���������� ��������,
                                                                       5 - ����� ���������� ��������,
                                                                       6 - ����� ���������� ��������,
                                                                       7 - ����� ���������� ����� ������,
                                                                       8 - ����� ���������� ����� ������}
    constructor Create(pl: TPoint; w, h: Integer; scX, scY: Byte; C: TCanvas); //�����������
    destructor Destroy; override;                //����������
    procedure Show;                              //����������� ����
    procedure WMIMOVING(var Msg: TMsg; var Handled: Boolean);  //���������� ��������� �� ������
    procedure GamePause(Pause: Boolean);         //������������/������������� ����
    procedure GameEnd(Who, Reason: Byte); overload; //����� ����
    procedure GameEnd; overload;                 //����� ����
    procedure GameStart;                         //������ ����
   published
    property PlaceMent: TPoint read GetPlaceMent write SetPlaceMent;
  end;

implementation

{ TGameField }
//����������� �������� ����
// pl: TPoint - ����������������� �����
// w, h: Integer - ������ � ������ �����
// scX, scY: Byte - ������ ����� �������� ���� �� ����������� � ���������
// C: TCanvas - ������, ���� ��������� �������
// cs: Byte - ���������� �����
constructor TGameField.Create(pl: TPoint; w, h: Integer; scX, scY: Byte; C: TCanvas);
var
  i: Byte;
begin
  //�������� ����������� � ������
  inherited Create;
  //�������������� ��������� ��������� �� ���������� � ����� ��������� (��� ��������� �������� ������)
  Application.OnMessage := WMIMOVING;
  //�������������� ��������� ����� ������
  PlaceMent := pl;                               //�������������� ����
  Width := w; Height := h;                       //������ � ������ ����
  SizeCellX := scX; SizeCellY := scY;            //������ ����� ���� �� ����������� � ���������
  CountCellX := Trunc(Width /SizeCellX);         //���������� ����� ���� �� �����������
  CountCellY := Trunc(Height /SizeCellY);        //���������� ����� ���� �� ���������
  Canvas := C;                                   //������
  //������������ �������� ����� ����
  for i := 0 to CountCellX + 1 do begin          //�������������� �����
    Map[i,0] := -1;
    Map[i,CountCellY + 1] := -1;
  end;
  for i := 0 to CountCellY + 1 do begin           //������������ �����
    Map[0,i] := -1;
    Map[CountCellX + 1,i] := -1;
  end;
end;

//���������� �������� ����
destructor TGameField.Destroy;
var
  i: Byte;
begin
  //������� ���������� ������
  for i := 1 to CountSnake do begin
    Snake[i].Free;
    Snake[i] :=nil;
  end;
  //���������� �������
  HumanGamer.Free;
  HumanGamer := nil;
  HumanGamer2.Free;
  HumanGamer2 := nil;
  ComputerGamer.Free;
  ComputerGamer := nil;
  //���������� ����
  inherited;
end;

//����������� �������� ����
procedure TGameField.Show;
var
  i, j: Byte;
  BrushColor: TColor;
begin
  //������ ���� � ���� �������������� �� �������
  Canvas.Rectangle(SizeCellX + PlaceMent.X, SizeCellY + PlaceMent.Y, SizeCellX + PlaceMent.X + Width, SizeCellY + PlaceMent.Y + Height);
  //������ � ���� �������, ������� ��������� ������
  BrushColor := Canvas.Brush.Color;
  Canvas.Brush.Color := clBlack;
  for i := 1 to CountCellX do begin
    for j := 1 to CountCellY do begin
      if (Map[i,j] = 1) or (Map[i,j] = 2) then begin
        Canvas.Rectangle(PlaceMent.X + SizeCellX * i, PlaceMent.Y + SizeCellY * j, PlaceMent.X + SizeCellX * (i + 1), PlaceMent.Y + SizeCellY * (j + 1));
      end;
    end;
  end;
  Canvas.Brush.Color := BrushColor;
  //���������� ������
  for i := 1 to CountSnake do
    Snake[i].Show;
  //����������� ������� (������������ �� ���� ������ ���� �� ����� 10 �������)
  for i := 1 to 10 do
    if Bonus[i] <> nil then
      Bonus[i].Show(False);
end;

//����� � ���� ��� ������������� ����
// Pause: Boolean - ������� ������������/������������� ����
procedure TGameField.GamePause(Pause: Boolean);
var
  i: Byte;
begin
  if Pause then                                  //���� ���� ������������� ����
    //��������� ������� � ���� �����
    for i := 1 to CountSnake do
      Snake[i].Timer.Enabled := False
  else                                           //���� ���� ����������� ����
    //�������� ������� � ���� �����
    for i := 1 to CountSnake do
      Snake[i].Timer.Enabled := True;
end;

//��������� ��������� �� ������ � �� ������������
//� ���� ��������� ������������ �������� ������� � ���������� ����������� �������� ������
procedure TGameField.WMIMOVING(var Msg: TMsg; var Handled: Boolean);
var
  CanMove: Integer;                              //��� ��������� ������ �� �������� ������
  NumSnake: Byte;                                //����� ������
  i: Byte;
begin
  if GameNow then begin                          //���� ���� ����
    //������������ �������� ���������
    if Msg.message = WM_IMOVING then begin       //���� ������ ��������� �� ������ � ������������� ����������� ��������
      //��������� ��������� � ���������� �����������
      NumSnake := Msg.wParam;                    //������ �� ����� ������ ������ ���������
      case Snake[NumSnake].Orientation of        //��� ��������� �� ����, ���� ������ ����� ����
      oNoth : CanMove := Map[Trunc(Snake[NumSnake].PlaceMent.X / SizeCellX), Trunc(Snake[NumSnake].PlaceMent.Y / SizeCellY) - 1];
      oSouth: CanMove := Map[Trunc(Snake[NumSnake].PlaceMent.X / SizeCellX), Trunc(Snake[NumSnake].PlaceMent.Y / SizeCellY) + 1];
      oWest : CanMove := Map[Trunc(Snake[NumSnake].PlaceMent.X / SizeCellX - 1), Trunc(Snake[NumSnake].PlaceMent.Y / SizeCellY)];
      oEast : CanMove := Map[Trunc(Snake[NumSnake].PlaceMent.X / SizeCellX + 1), Trunc(Snake[NumSnake].PlaceMent.Y / SizeCellY)];
      end;
      //� ����������� �� ����, ��� ��������� �������, �� ���-�� ������
      case CanMove of
      -1 : begin                                   //���� ������� �����
             //����������� ���� - ����� ��������
             Collision(NumSnake, 1);
           end;
      0  : begin                                   //���� ������� �� ���� ���
             //�������� ������� ��������� ������ �� �����
             for i := 1 to Snake[NumSnake].CountBits do
               Map[Trunc(Snake[NumSnake].SnakeBits[i].PlaceMent.X / SizeCellX), Trunc(Snake[NumSnake].SnakeBits[i].PlaceMent.Y / SizeCellY)] := 0;
             //������������ ������
             Snake[NumSnake].Move;
             //��������� ����� ��������� ������ �� �����
             for i := 1 to Snake[NumSnake].CountBits do
               Map[Trunc(Snake[NumSnake].SnakeBits[i].PlaceMent.X / SizeCellX), Trunc(Snake[NumSnake].SnakeBits[i].PlaceMent.Y / SizeCellY)] := NumSnake;
           end;
      1  : begin                                   //���� ������� ���� ������, �.�. ����� �����
             //����������� ���� - ����� ��������
             Collision(NumSnake, 1);
           end;
      2  : begin                                   //���� ������� ������ ������
             //����������� ���� - ����� ��������
             Collision(NumSnake, 1);
           end;
      3  : begin                                   //���� ������� ����� ���������� ��������
             //��������� ������������
             Collision(NumSnake, 3);
           end;
      4  : begin                                   //���� ������� ����� ���������� ��������
             //��������� ������������
             Collision(NumSnake, 4);
           end;
      5  : begin                                   //���� ������� ����� ���������� ��������
             //��������� ������������
             Collision(NumSnake, 5);
           end;
      6  : begin                                   //���� ������� ����� ���������� ��������
             //��������� ������������
             Collision(NumSnake, 6);
           end;
      7  : begin                                   //���� ������� ����� ���������� ����� ������
             //��������� ������������
             Collision(NumSnake, 7);
           end;
      8  : begin                                   //���� ������� ����� ���������� ����� ������
             //��������� ������������
             Collision(NumSnake, 8);
           end;
      end;
      Handled := true;
    end;
  end;
end;

//����� ����
// Who: Byte - ��� �����������
// Reason: Byte - ������� ��������� ����: 0 - ����� ����, 1 - ������������ �� �������, � ��. �������, � ����� �����, 2 - ������ �����
procedure TGameField.GameEnd(Who, Reason: Byte);
var
  i, j: Byte;
begin
  //��������� ������� ��������� ����
  case Reason of
{  0: begin                                       //���������� ����� ����
     end;    }
  1, 2: begin                                       //����������� �� �������, ���� � ����� ��� � ������ �������
       case GameType of
       gtOnce            : begin                 //����� ���� �����
                             HumanGamer.Status := sLoser;            //����� ��������
                           end;
       gtOnceWithComputer: begin                 //����� ����� � �����������
                             case Who of         //�������� ��� ��������
                             1: begin
                                HumanGamer.Status := sLoser;         //����� ��������
                                ComputerGamer.Status := sWinner;     //��������� �������
                                end;
                             2: begin
                                HumanGamer.Status := sWinner;        //����� �������
                                ComputerGamer.Status := sLoser;      //��������� ��������
                                end;
                             end;
                           end;
       gtTwiceOnComputer, gtTwiceOnNetwork: begin //������ ��� ������ �� ����� ���������� ��� �� ����
                                              case Who of    //�������� ��� ��������
                                              1: begin
                                                 HumanGamer.Status := sLoser;   //������ ����� ��������
                                                 HumanGamer2.Status := sWinner; //������ ����� �������
                                                 end;
                                              2: begin
                                                 HumanGamer.Status := sWinner;  //������ ����� �������
                                                 HumanGamer2.Status := sLoser;  //������ ����� ��������
                                                 end;
                                              end;
                                            end;
       end;
     end;
{  2: begin                                       //������ ����� ������ �����
     end; }
  end;
  //����� ����
  GameNow := False;
  //����������� �����
  for i := 1 to CountSnake do begin
    Snake[i].Free;
    Snake[i] := nil;
  end;
  //��������� �����
  for i := 1 to CountCellX do
    for j := 1 to CountCellY do
      Map[i, j] := 0;
  //���� ��������, �������� �����
  case GameType of
  gtOnce            : begin                    //���� �����
                        case HumanGamer.Status of
                        sWinner: begin         //������� �������
                                   ShowMessage('�� ��������!!!' + #13 + '������� �����: ' + IntToStr(HumanGamer.Score));
                                 end;
                        sLoser : begin          //������� ��������
                                   ShowMessage('�� ���������!!!' + #13 + '������� �����: ' + IntToStr(HumanGamer.Score));
                                 end;
                        sNone  : ;
                        end;
                      end;
  gtOnceWithComputer: begin                    //������� ������ ����������
                      end;
  gtTwiceOnComputer : begin                    //��� �������� �� ����� ����������
                        if HumanGamer.Status = sWinner then
                          ShowMessage('������� ' + HumanGamer.Name +  #13 + '������� �����: ' + IntToStr(HumanGamer.Score))
                        else
                          ShowMessage('������� ' + HumanGamer2.Name +  #13 + '������� �����: ' + IntToStr(HumanGamer2.Score))
                      end;
  gtTwiceOnNetwork  : begin                    //��� �������� �� ����
                      end;
  end;
end;

//����� ����
procedure TGameField.GameEnd;
var
  i, j: Byte;
begin
  //����� ����
  GameNow := False;
  //����������� �����
  for i := 1 to CountSnake do begin
    Snake[i].Free;
    Snake[i] := nil;
  end;
  //��������� �����
  for i := 1 to CountCellX do
    for j := 1 to CountCellY do
      Map[i, j] := 0;
end;

//������ ����
procedure TGameField.GameStart;
var
  i, j: Byte;
  BonusMode: Byte;                               //��� ������ (3 - 8)
  BonusPlace: TPoint;                            //�������������� ������
begin
  //���������� ����� �� ���� ����
  case GameType of
  gtOnce            : CountSnake := 1;
  gtOnceWithComputer: CountSnake := 2;
  gtTwiceOnComputer : CountSnake := 2;
  gtTwiceOnNetwork  : CountSnake := 2;
  end;
  //�������� ������
  for i := 1 to CountSnake do begin
    //�������� ������
    Snake[i] := TSnake.Create(Point(10 * i * SizeCellX, 20 * SizeCellY), SizeCellX, SizeCellY, 5, Canvas, PlaceMent.X, PlaceMent.Y, i);
    //�������� �� ������� �� ����� �������� ����
    for j := 1 to Snake[i].CountBits do
      Map[10 * i, 20 + (j - 1)] := i;
  end;
  //�������� ������� (�� ���� ������������ ����� ���� ������ 10 �������)
  for i := 1 to 10 do begin
    AddBonus(i);
  end;
  //������� ������ ����
  GameNow := True;
  //������������ ����
  Show;
end;

//��������� ����� ������������ �� ����
// Who: Byte - ��� ������
// Reason: Byte - � ��� ������������
procedure TGameField.Collision(Who, Reason: Byte);
var
  IndexBonus: Byte;
function FindBonus: Byte;                        //��������� ������� (������ ��� ��������� Collision) ��� ����������� ������� ������, � ������� ��������� ������������
var
  i: Byte;
begin
  for i := 1 to 10 do begin                      //������������� ��� ������ �� ���� (������� ������ 10 ����)
    if Bonus[i] <> nil Then begin                //���� ����� ����������, �� ���������� ��� �������������� � ��������������� ������
      case Snake[Who].Orientation of                 //��� ��������� �� ����, ���� ������ ����� ����
      oNoth : begin
                if Bonus[i].PlaceMent.Y = Snake[Who].PlaceMent.Y - SizeCellY then begin
                  Result := i;
                  break;
                end;
              end;
      oSouth: begin
                if Bonus[i].PlaceMent.Y = Snake[Who].PlaceMent.Y + SizeCellY then begin
                  Result := i;
                  break;
                end;
              end;
      oWest : begin
                if Bonus[i].PlaceMent.X = Snake[Who].PlaceMent.X - SizeCellX then begin
                  Result := i;
                  break;
                end;
              end;
      oEast : begin
                if Bonus[i].PlaceMent.X = Snake[Who].PlaceMent.X + SizeCellX then begin
                  Result := i;
                  break;
                end;
              end;
      end;
    end;
  end;
end;
begin
  case Reason of
  1    : begin                                   //������� - ����� ��� ���� ������, ��� ����� ������
           //����������� ������
           GameEnd(Who, 1);
         end;
  3..8 : begin                                   //������� - �����-�� �����
           //����������� ������� ������, � ��� �����������
           IndexBonus := FindBonus;
           //����� �������� ����� ������
           Bonus[IndexBonus].Action(Snake[Who]);
           //��������� ����� ������
           case GameType of
           gtOnce            : begin
                                 HumanGamer.Score := HumanGamer.Score + Bonus[IndexBonus].Ball;
                               end;
           gtOnceWithComputer: begin
                                 case Who of
                                 1: HumanGamer.Score := HumanGamer.Score + Bonus[IndexBonus].Ball;
                                 2: ComputerGamer.Score := ComputerGamer.Score + Bonus[IndexBonus].Ball;
                                 end;
                               end;
           gtTwiceOnComputer,
           gtTwiceOnNetwork  : begin
                                 case Who of
                                 1: HumanGamer.Score := HumanGamer.Score + Bonus[IndexBonus].Ball;
                                 2: HumanGamer2.Score := HumanGamer2.Score + Bonus[IndexBonus].Ball;
                                 end;
                               end;
           end;
           //�������� ��������� ���� �� ������� ������ ������
           if not Snake[Who].ALive then begin
             GameEnd(Who, 2);
           end;
           //������� ����� �������� ���� �� ��� �����, ��� ��� �����
           Map[Trunc(Bonus[IndexBonus].PlaceMent.X / SizeCellX), Trunc(Bonus[IndexBonus].PlaceMent.Y / SizeCellY)] := 0;
           //����������� ������
           RemoveBonus(IndexBonus);
           //���������� � ����������� ������ � ����� �����
           AddBonus(IndexBonus);
           Bonus[IndexBonus].Show(False);
         end;                                         
  end;
end;

//���������� ���������� ������
// Index: Byte - ����� ������������ ������
// AddBonus: TPoint - �������������� ������ ������
function TGameField.AddBonus(Index: Byte): TPoint;
var
  BonusMode: Byte;                               //��� ������ (3 - 8)
  BonusPlace: TPoint;                            //�������������� ������
begin
  //�������� ��� ������ ��������� ������� (����� �� 3 �� 8)
  BonusMode := Random(6) + 3;
  //��������� ������� �������� ��������� ������ �� ���� (����� �������� ������ ���� ������)
  //���������� �������������� ������ �� ��� ���, ���� �� ���� � ���� ������ �� ����� ������ �����
  repeat
    BonusPlace := Point(Random(CountCellX) + 1, Random(CountCellY) + 1);
  until Map[BonusPlace.X, BonusPlace.Y] = 0;
  //������������ ����� �������� ����, ������� ����� ��������� ����
  Map[BonusPlace.X, BonusPlace.Y] := BonusMode;
  //����������� �������������� ������ � ���������� ���������� ����
  BonusPlace.X := (BonusPlace.X) * SizeCellX;
  BonusPlace.Y := (BonusPlace.Y) * SizeCellY;
  //� ����������� �� ���� ������ ������� ��,
  //������ �������� ������ � ���������� �� �������������� �������� ������� ����
  try
    case BonusMode of
    3: begin                                     //����� ���������� ��������
         Bonus[Index] := TBonusSpeed.Create(BonusPlace, PlaceMent.X, PlaceMent.Y, Canvas, SizeCellX, SizeCellY, BonusMode, True);
       end;
    4: begin                                     //����� ���������� ��������
         Bonus[Index] := TBonusSpeed.Create(BonusPlace, PlaceMent.X, PlaceMent.Y, Canvas, SizeCellX, SizeCellY, BonusMode, False);
       end;
    5: begin                                     //����� ���������� ��������
         Bonus[Index] := TBonusHealth.Create(BonusPlace, PlaceMent.X, PlaceMent.Y, Canvas, SizeCellX, SizeCellY, BonusMode, True);
       end;
    6: begin                                     //����� ���������� ��������
         Bonus[Index] := TBonusHealth.Create(BonusPlace, PlaceMent.X, PlaceMent.Y, Canvas, SizeCellX, SizeCellY, BonusMode, False);
       end;
    7: begin                                     //����� ���������� �����
         Bonus[Index] := TBonusLength.Create(BonusPlace, PlaceMent.X, PlaceMent.Y, Canvas, SizeCellX, SizeCellY, BonusMode, True);
       end;
    8: begin                                     //����� ���������� �����
         Bonus[Index] := TBonusLength.Create(BonusPlace, PlaceMent.X, PlaceMent.Y, Canvas, SizeCellX, SizeCellY, BonusMode, False);
       end;
    end;
    Result := Bonus[Index].PlaceMent;
  except
    //������������ ������� ����� �������� ����, ���� ����� �� ������� �������
    Map[Trunc(BonusPlace.X / SizeCellX), Trunc(BonusPlace.Y / SizeCellY)] := BonusMode;
  end;
end;

//�������� ���������� ������
// Index: Byte - ����� ������������ ������
procedure TGameField.RemoveBonus(Index: Byte);
begin
  Bonus[Index].Free;
  Bonus[Index] := nil;
end;

//��� ������ �������� �������� "��������������"
function TGameField.GetPlaceMent: TPoint;
begin
  Result := FPlaceMent;
end;

//��� ������ �������� �������� "��������������"
procedure TGameField.SetPlaceMent(ANewValue: TPoint);
begin
  if ((ANewValue.X >= 0) and (ANewValue.X < 100)) and
     ((ANewValue.Y >= 0) and (ANewValue.Y < 100)) then
    FPlaceMent := ANewValue;
end;

{ TSnakeBits }
//����������� ����� ������
// pl: TPoint - ����������������� �����
// w, h: Integer - ������ � ������ �����
// dX, dY: Integer - �������� �������� ���� ������������ �����
// C: TCanvas - ��������� ��� ��������
constructor TSnakeBits.Create(pl: TPoint; w, h: Integer; c: TCanvas; ddX, ddY: Integer);
begin
  //����� ������������ ������
  inherited Create;
  //��������� ��������� �������� ����� �������
  PlaceMent := pl;                               //�������������� �����
  Width := w; Height := h;                       //������ � ������ ����
  dX := ddX; dY := ddY;                          //�������������� �������� ����
  Color := clBlack;
  Orientation := oNoth;                          //����� ���������� - ������� ������ �����
  Canvas := c;                                   //������
end;

//������������ ����� ������
// Orient: TOrientation - ���� ������ (�������� ����� ������� ������ �� ���������� ����������� ����� �� ���������� ����)
// Replace: TPoint - ���������� ���������� ������ ������ ���������
function TSnakeBits.Replace(Orient: TOrientation): TPoint;
begin
  Orientation := Orient;                         //����� ����������� �������� ����� (����� ��������� �� ������)
  Case Orient Of                                 //� ����������� �� ����������� ���������
  oNoth : begin
            PlaceMent.Y := PlaceMent.Y - Height;
          end;
  oSouth: begin
             PlaceMent.Y := PlaceMent.Y + Height;
         end;
  oWest : begin
            PlaceMent.X := PlaceMent.X - Width;
          end;
  oEast : begin
            PlaceMent.X := PlaceMent.X + Width;
          end;
  end;
  Result := PlaceMent;
end;

//����������� ����� ������:
// Transparent: Boolean - ������� ������������ �����
procedure TSnakeBits.Show(Transparent: Boolean);
var
  BrushColorOld, PenColorOld: TColor;
begin
  //���������� ������� �����
  BrushColorOld := Canvas.Brush.Color;
  PenColorOld := Canvas.Pen.Color;
  //��������� ����� ���� � �����
  if Transparent then begin                      //���� �������� ���������� ����� - ���� ����
    Canvas.Brush.Color := clBtnFace;
    Canvas.Pen.Color := clBtnFace;
  end
  else begin                                     //���� �������� ������������ �����
    Canvas.Brush.Color := Color;
    Canvas.Pen.Color := clBlack;
  end;
  //���������
  Canvas.Rectangle(dX + PlaceMent.X, dY + PlaceMent.Y, dX + PlaceMent.X + Width, dY + PlaceMent.Y + Height);
  //�������������� ������� �����
  Canvas.Brush.Color := BrushColorOld;
  Canvas.Pen.Color := PenColorOld;
end;

{ TSnake }
//����������� ������
// pl: TPoint - ����������������� �����
// scX, scY: Byte - ������ ����� �� ��������� � �����������
// cb: Byte - ���������� ������� ������
// C: TCanvas - ��������� ��� ��������
// dX, dY: Integer - �������� �������� ���� ������������ �����
// t: Integer - ����� ������
constructor TSnake.Create(pl: TPoint; scX, scY: Byte; cb: Byte; C: TCanvas; ddX, ddY: Integer; t: Integer);
var
  i: Byte;
begin
  //�������� ����������� ������
  inherited Create;
  //������������� ��������� �������� ����� �������
  PlaceMent := pl;                               //�������������� ����
  SizeCellX := scX; SizeCellY := scY;            //������ � ������ ������
  CountBits := cb;                               //���������� ������� � ������
  Canvas := C;                                   //������
  dX := ddX; dY := ddY;                          //�������� �������� ����
  Tag := t;                                      //���������� ����� ������
  Health := 100;                                 //��������
  Orientation := oNoth;                          //���������� � ������������
  FSpeed := 500;                                 //�������� � ������������ ��� �������
  FALive := True;                                //������ ����
  //������� ������ �� �������
  for i := 1 to CountBits do
    SnakeBits[i] := TSnakeBits.Create(Point(PlaceMent.X,PlaceMent.Y + SizeCellY * (i - 1)), SizeCellX, SizeCellY, Canvas, dX, dY);
  //������� ������ � ������������� ��������� ��������
  Timer := TTimer.Create(nil);                   //������� ������
  Timer.Interval := Speed;                       //�������� ������������
  Timer.OnTimer := INeedMove;                    //��������� ������� OnTimer ������������� ���� ���������
  Timer.Enabled := True;                         //�������� ������
end;

//���������� ������
destructor TSnake.Destroy;
var
  i: Byte;
begin
  //������� ���������� ������
  Timer.Enabled := False;                        //������������� ������
  Timer.Free;                                    //���������� ������
  //����� ���������� ��� ������ ������
  for i := 1 to CountBits do begin
    SnakeBits[i].Free;
    SnakeBits[i] := nil;
  end;  
  //���������� ����
  inherited;
end;

//������� �������
// Sender: TObject - ���� ��� �������
procedure TSnake.INeedMove(Sender: TObject);
begin
  //�������� ��������� ����, ��� ������ ������ �����
  PostMessage(Application.Handle,WM_IMOVING,Tag,0);
end;

//������������ ������
procedure TSnake.Move;
var
  i: Byte;
  PrevOrient, PrevPrevOrient: TOrientation;      //����������� �������� ����������� ����� ������
begin
  //����������� �� ������ �����
  for i := 1 to CountBits do
    SnakeBits[i].Show(True);
  //���������� ��� ������
  PrevPrevOrient := Orientation;                 //���������� ���������� ����������� �������� ������
  for i := 1 to CountBits do begin
    PrevOrient := SnakeBits[i].Orientation;      //���������� ���������� ����������� �������� ������� ����� ������
    SnakeBits[i].Replace(PrevPrevOrient);        //���������� ������ ����� ������, �������� ����� ����������� ��������
    PrevPrevOrient := PrevOrient;                //���������� ���������� ����������� �������� ������
  end;
  //�������� ���� �������������� (������)
  Case Orientation Of                            //������ �� ����������� ��������
  oNoth : begin
            PlaceMent.Y := PlaceMent.Y - SizeCellY;
          end;
  oSouth: begin
             PlaceMent.Y := PlaceMent.Y + SizeCellY;
         end;
  oWest : begin
            PlaceMent.X := PlaceMent.X - SizeCellX;
          end;
  oEast : begin
            PlaceMent.X := PlaceMent.X + SizeCellX;
          end;
  end;
  //���������� �� ����� �����
  for i := 1 to CountBits do
    SnakeBits[i].Show(False);
end;

//����������� ������
procedure TSnake.Show;
var
  i: Byte;
begin
  //���������� ������ �� �������
  for i := 1 to CountBits do begin
    SnakeBits[i].Show(False);
  end;
end;

//���������� ����� ������
// Index: Byte - ����� ������������ �����
procedure TSnake.AddBits(Index: Byte);
begin
  //��������� �����
  //� ����������� �� ���������� ����������� ����� ������
  case SnakeBits[Index - 1].Orientation of
  oNoth : begin
            //��������� �����
            SnakeBits[Index] := TSnakeBits.Create(Point(SnakeBits[Index - 1].PlaceMent.X, SnakeBits[Index - 1].PlaceMent.Y + SizeCellY), SizeCellX, SizeCellY, Canvas, dX, dY);
            //������������� ����������
            SnakeBits[Index].Orientation := oNoth;
          end;
  oSouth: begin
            //��������� �����
            SnakeBits[Index] := TSnakeBits.Create(Point(SnakeBits[Index - 1].PlaceMent.X, SnakeBits[Index - 1].PlaceMent.Y - SizeCellY), SizeCellX, SizeCellY, Canvas, dX, dY);
            //������������� ����������
            SnakeBits[Index].Orientation := oSouth;
          end;
  oWest : begin
            //��������� �����
            SnakeBits[Index] := TSnakeBits.Create(Point(SnakeBits[Index - 1].PlaceMent.X + SizeCellX, SnakeBits[Index - 1].PlaceMent.Y), SizeCellX, SizeCellY, Canvas, dX, dY);
            //������������� ����������
            SnakeBits[Index].Orientation := oWest;
          end;
  oEast : begin
            //��������� �����
            SnakeBits[Index] := TSnakeBits.Create(Point(SnakeBits[Index - 1].PlaceMent.X - SizeCellX, SnakeBits[Index - 1].PlaceMent.Y), SizeCellX, SizeCellY, Canvas, dX, dY);
            //������������� ����������
            SnakeBits[Index].Orientation := oEast;
          end;
  end;
end;

//�������� ����� ������
// Index: Byte - ����� ���������� �����
procedure TSnake.RemoveBits(Index: Byte);
begin
  //���������� ��������� �����
  SnakeBits[Index].Free;
  SnakeBits[Index] := nil;
end;

//��� ������ ������ �������� �������� "��������"
// ANewValue: Integer - ����� �������� �������� "��������"
// ����� ��������� ����� �������� ��������, ���� ��� ������ 100, �� �� ��������� ������,
//���� ��� ������ 1000, �� ������ �� �����������. ����� ���� ��������,
//�������� �������� ��������� �������. ���� ����� �������� ������ 1000, �� ������ �������
procedure TSnake.SetSpeed(ANewValue: Integer);
begin
  //��������� �� ��������� � ���������� ��������
  if (ANewValue >= 100) and (ANewValue <= 1000) then begin
    FSpeed := ANewValue;                         //����� �������� �������� ������
    Timer.Interval := FSpeed;                    //��������� ��������� �������
  end;
  if ANewValue > 1000 then begin                 //���� ����� �������� ������ 1000
    ALive := False;                              //������ �������
  end;
end;

//��� ������ ������ �������� �������� "��������"
// ANewValue: Integer - ����� �������� �������� "��������"
// ����� ��������� ����� �������� ��������, ���� ��� ������ 0, �� �� ��������� ������
//� ������ �������
procedure TSnake.SetHealth(ANewValue: Integer);
begin
  //��������� �� ��������� � ���������� ��������
  if ANewValue > 0 then begin
    FHealth := ANewValue;                        //����� �������� �������� ������
  end
  else begin
    ALive := False;                              //������ �������
  end;
end;

//��� ������ ������ �������� �������� "�����"
// ANewValue: Integer - ����� �������� �������� "�����"
// ����� ��������� ����� �������� �����, ���� ��� ������ 0, �� �� ��������� ������
//� ������ �������. ���� ������ 100, �� ������ �� �����������
procedure TSnake.SetCountBits(ANewValue: Byte);
begin
  //��������� �� ��������� � ���������� ��������
  if (ANewValue > 0) and (ANewValue < 100) then begin
    FCountBits := ANewValue;                     //����� �������� ����� ������
  end;
  if ANewValue < 1 then begin
    ALive := False;                              //������ �������
  end;
end;

{ TGamer }
//�����������
constructor TGamer.Create;
begin
  //�������� ����������� ������
  inherited Create;
  //������������� ��������� �������� ����� �������
  FName := '�����1';                             //��� ������
  FScore := 0;                                   //����
  Status := sNone;                               //������ ������
end;

{ TBonus }
//�����������
// pl: TPoint - �������������� ������
// ddx,ddy: Integer - �������� �������� ���� ������������ �����
// c: TCanvas - ��� ����� ��������
// scx, scy: Byte - ������ ����� �� ����������� � ���������
// t: Byte - ������������� ������
// Up: Boolean - �������� ������ (���������� ���������� ��� ����������), �� ��������� - ����������
constructor TBonus.Create(pl: TPoint; ddx,ddy: Integer; c: TCanvas; scx, scy: Byte; t: Byte; Up: Boolean = True);
begin
  //�������� ����������� � ������
  inherited Create;
  //�������������� ���������
  PlaceMent := pl;                               //�������������� ������
  dX := ddx; dY := ddy;                          //�������� �������� ����
  Canvas := c;                                   //�����
  SizeCellX := scx; SizeCellY := scy;            //������� ����� �������� ���� �, ��������������, ������
  Tag := t;                                      //������������� ������
end;

//����������� ������
// Transparent: Boolean - ������� ������������ ������
procedure TBonus.Show(Transparent: Boolean);
var
  BrushColorOld, PenColorOld: TColor;
begin
  //���������� ������� �����
  BrushColorOld := Canvas.Brush.Color;
  PenColorOld := Canvas.Pen.Color;
  //��������� ����� ���� � �����
  if Transparent then begin                      //���� �������� ���������� ����� - ���� ����
    Canvas.Brush.Color := clBtnFace;
    Canvas.Pen.Color := clBtnFace;
  end
  else begin                                     //���� �������� ������������ �����
    Canvas.Brush.Color := Color;
    Canvas.Pen.Color := clBlack;
  end;
  //���������
  Canvas.Rectangle(dX + PlaceMent.X, dY + PlaceMent.Y, dX + PlaceMent.X + SizeCellX, dY + PlaceMent.Y + SizeCellY);
  //�������������� ������� �����
  Canvas.Brush.Color := BrushColorOld;
  Canvas.Pen.Color := PenColorOld;
end;

{ TBonusSpeed }
//�������� ��� ������������
// Snake: TSnake - ��������� �� ������, � ��� �����������
// Action: Integer - ������� ���������� ����� �������� ��������
function TBonusSpeed.Action(Snake: TSnake): Integer;
begin
  //��������� ���������� �������� � ������ (���� "-" ������ � ���, ��� �������� ������ - ��� �������� ������������ �������)
  Snake.Speed := Snake.Speed - Value;
  Result := Snake.Speed;
end;

//�����������
// pl: TPoint - �������������� ������
// ddx,ddy: Integer - �������� �������� ���� ������������ �����
// c: TCanvas - ��� ����� ��������
// scx, scy: Byte - ������ ����� �� ����������� � ���������
// t: Byte - ������������� ������
// Up: Boolean - �������� ������ (���������� ���������� ��� ����������), �� ��������� - ����������
constructor TBonusSpeed.Create(pl: TPoint; ddx, ddy: Integer; c: TCanvas; scx, scy, t: Byte; Up: Boolean = True);
begin
  //�������� ����������� ������
  inherited Create(pl, ddx, ddy, c, scx, scy, t);
  //��������� ���� � ����������� �� ��������� ������
  if Up then begin                               //���� ����� ���������� ��������
    Value := 100;                                //������� ���������� ��������
    Color := clGreen;                            //���� ������ - �������
    Ball := 10;                                  //����, ������� ���� ������ �����
  end
  else begin                                     //���� ����� ���������� ��������
    Value := -100;                               //������� ���������� ��������
    Color := clRed;                              //���� ������ - �������
    Ball := -5;                                  //����, ������� ���� ������ �����
  end;
end;

{ TBonusHealth }
//�������� ��� ������������
// Snake: TSnake - ��������� �� ������, � ��� �����������
// Action: Integer - ������� ���������� ����� �������� ��������
function TBonusHealth.Action(Snake: TSnake): Integer;
begin
  //��������� ���������� �������� � ������
  Snake.Health := Snake.Health + Value;
  Result := Snake.Health;
end;

//�����������
// pl: TPoint - �������������� ������
// ddx,ddy: Integer - �������� �������� ���� ������������ �����
// c: TCanvas - ��� ����� ��������
// scx, scy: Byte - ������ ����� �� ����������� � ���������
// t: Byte - ������������� ������
// Up: Boolean - �������� ������ (���������� ���������� ��� ����������), �� ��������� - ����������
constructor TBonusHealth.Create(pl: TPoint; ddx, ddy: Integer; c: TCanvas;
  scx, scy, t: Byte; Up: Boolean);
begin
  //�������� ����������� ������
  inherited Create(pl, ddx, ddy, c, scx, scy, t);
  //��������� ���� � ����������� �� ��������� ������
  if Up then begin                               //���� ����� ���������� ��������
    Value := 10;                                 //������� ���������� ��������
    Color := clLime;                             //���� ������ - ����
    Ball := 10;                                  //����, ������� ���� ������ �����
  end
  else begin                                     //���� ����� ���������� ��������
    Value := -10;                                //������� ���������� ��������
    Color := clBlue;                             //���� ������ - �����
    Ball := -5;                                  //����, ������� ���� ������ �����
  end;
end;

{ TBonusLength }
//�������� ��� ������������
// Snake: TSnake - ��������� �� ������, � ��� �����������
// Action: Integer - ������� ���������� ����� �������� �����
function TBonusLength.Action(Snake: TSnake): Integer;
begin
  //��������� ���������� ����� � ������. ���������� ����������� ��� ����������� ���������� �������
  if Value > 0 then begin                        //���������� ����� ������
    Snake.CountBits := Snake.CountBits + Value;
    Snake.AddBits(Snake.CountBits);
  end
  else begin                                     //�������� ����� ������
    Snake.RemoveBits(Snake.CountBits);
    Snake.CountBits := Snake.CountBits + Value;
  end;
  Result := Snake.CountBits;
end;

constructor TBonusLength.Create(pl: TPoint; ddx, ddy: Integer; c: TCanvas;
  scx, scy, t: Byte; Up: Boolean);
begin
  //�������� ����������� ������
  inherited Create(pl, ddx, ddy, c, scx, scy, t);
  //��������� ���� � ����������� �� ��������� ������
  if Up then begin                               //���� ����� ���������� �����
    Value := 1;                                  //������� ���������� �����
    Color := clOlive;                            //���� ������ - ����
    Ball := 10;                                  //����, ������� ���� ������ �����
  end
  else begin                                     //���� ����� ���������� �����
    Value := -1;                                 //������� ���������� �����
    Color := clPurple;                           //���� ������ - �����
    Ball := -5;                                  //����, ������� ���� ������ �����
  end;
end;

end.
