unit InitGame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Main, SnakeUnit;

type
  TForm1 = class(TForm)
    RadioGroup1: TRadioGroup;
    Button1: TButton;
    Button2: TButton;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

//�����
procedure TForm1.Button2Click(Sender: TObject);
begin
  Close;
end;

//��������� ���� ����
procedure TForm1.Button1Click(Sender: TObject);
begin
  case RadioGroup1.ItemIndex of
  0: GameField.GameType := gtOnce;
  1: GameField.GameType := gtOnceWithComputer;
  2: GameField.GameType := gtTwiceOnComputer;
  3: GameField.GameType := gtTwiceOnNetwork;
  else
     GameField.GameType := gtNone;
  end;
end;

end.
