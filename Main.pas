unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, SnakeUnit;

type
  //�����
  TForm2 = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button5: TButton;
    Bevel1: TBevel;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button5Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormPaint(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;
  GameField: TGameField;                         //������� ����
  GameFieldPlaceMent: TPoint;                    //�������� ���� - �������������� �������� ����
  GameFieldWidth: Integer;                       //�������� ���� - ������ �������� ����
  GameFieldHeight: Integer;                      //�������� ���� - ������ �������� ����
  GameFieldSizeCellX: Integer;                   //�������� ���� - ������ ����� ������ �������� ����
  GameFieldSizeCellY: Integer;                   //�������� ���� - ������ ����� ������ �������� ����

implementation

uses InitGame, Options, Types;
{$R *.dfm}

//������������� ������ �� ���������
procedure TForm2.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if MessageDlg('����� �� ����?', mtConfirmation, [mbYes,mbNo], 0) = mrYes then begin  //���� ���� ����� �� ����
    //����������� ����
    GameField.GameEnd;                           //����������� ����
    GameField.Free;                              //���������� ������� ����
    CanClose := True;
  end
  else
    CanClose := False;
end;

//�����
procedure TForm2.Button3Click(Sender: TObject);
begin
  Close;
end;

//�������� �����
procedure TForm2.FormCreate(Sender: TObject);
begin
  Randomize;
  //���������� ���������� ���� �� ���������
  GameFieldPlaceMent := Point(10, 10);           //�������� ���� - �������������� �������� ����
  GameFieldWidth := 500;                         //�������� ���� - ������ �������� ����
  GameFieldHeight := 400;                        //�������� ���� - ������ �������� ����
  GameFieldSizeCellX := 10;                      //�������� ���� - ������ ����� ������ �������� ����
  GameFieldSizeCellY := 10;                      //�������� ���� - ������ ����� ������ �������� ����
  //������� ������ - ������� ����
  GameField := TGameField.Create(GameFieldPlaceMent, GameFieldWidth, GameFieldHeight, GameFieldSizeCellX, GameFieldSizeCellY, Form2.Canvas);
end;

//�������� ����� ����
procedure TForm2.Button1Click(Sender: TObject);
begin
  //�������������� ���� � ����� ����
  GameField.GameEnd;                             //����������� ���� (����, ���� ��� ������ � �������� ����� ����)
  GameField.HumanGamer.Free;                     //���������� ���� ������� (����, ���� ��� ������ � �������� ����� ����)
  GameField.HumanGamer := nil;
  GameField.HumanGamer2.Free;
  GameField.HumanGamer2 := nil;
  GameField.ComputerGamer.Free;
  GameField.ComputerGamer := nil;
  //���������� ���� ������ ���� ����
  if Form1.ShowModal = mrOk then begin           //���� ����������� ������
    case GameField.GameType of
    gtOnce            : begin                    //���� �����
                          //��������� ����������
                          GroupBox1.Visible := True;
                          GroupBox2.Visible := False;
                          //�������� ������
                          GameField.HumanGamer := THumanGamer.Create;
                          //������ ����� ������
                          GameField.HumanGamer.Name := InputBox('��� ������', '������� ��� ������', '�����1');
                          //����������� ����� ������ �� �����
                          Label1.Caption := '���: ' + GameField.HumanGamer.Name;
                        end;
    gtOnceWithComputer: begin                    //������� ������ ����������
                        end;
    gtTwiceOnComputer : begin                    //��� �������� �� ����� ����������
                          //��������� ����������
                          GroupBox1.Visible := True;
                          GroupBox2.Visible := True;
                          //�������� �������
                          GameField.HumanGamer := THumanGamer.Create;
                          GameField.HumanGamer2 := THumanGamer.Create;
                          //������ ����� �������
                          GameField.HumanGamer.Name := InputBox('��� ������', '������� ��� ������', '�����1');
                          GameField.HumanGamer2.Name := InputBox('��� ������', '������� ��� ������', '�����2');
                          //����������� ����� ������� �� �����
                          Label1.Caption := '���: ' + GameField.HumanGamer.Name;
                          Label4.Caption := '���: ' + GameField.HumanGamer2.Name;
                        end;
    gtTwiceOnNetwork  : begin                    //��� �������� �� ����
                        end;
    gtNone            : begin                    //�� ������� ����
                          //����� �� ����
                          Close;
                        end;
    end;
    //����� ����
    GameField.GameStart;
  end;
end;

//������ - "�����/�����������"
procedure TForm2.Button2Click(Sender: TObject);
begin
  if GameField.GameNow then begin                //���� ���� ����
    if Button2.Caption = '�����' then begin      //���� ������� "�����"
      GameField.GamePause(True);                 //���������������� ����
      Button2.Caption := '�����������';
    end
    else begin                                   //���� ������� "�������������"
      GameField.GamePause(False);                //������������ ����
      Button2.Caption := '�����';
    end;
  end
  else begin                                     //���� ���� ���, �� ������ �������������
    ShowMessage('������ �� ��������, ���� �� �������� ����.');
  end;
end;

//����������� ����� � �������� ����
procedure TForm2.FormPaint(Sender: TObject);
begin
  if GameField.GameNow then                      //���� ���� ����, ����� �������� ������ �� ����
    GameField.Show;                              //�������������� ������� ����
end;

//������� ������
procedure TForm2.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if GameField.GameNow then begin                //���� ���� ����, ����� ����������� �� ������� ������ �� ����
    //�������� ���������� �������� ������
    Case Key Of                                  //����� ������� ������
    //���������� ������ �������, ��������� ���������� ������ � ������������
    38{VK_UP}, 80{VK_P}    : GameField.Snake[1].Orientation := oNoth;
    40{VK_DOWN}, 190{VK_>} : GameField.Snake[1].Orientation := oSouth;
    37{VK_LEFT}, 76{VK_L}  : GameField.Snake[1].Orientation := oWest;
    39{VK_RIGHT}, 186{VK_:}: GameField.Snake[1].Orientation := oEast;
    //���������� ������ �������, ��������� ���������� ������ � ������������
    87{VK_W}    : if GameField.CountSnake > 1 then    //���� � ���� ��� ������
                GameField.Snake[2].Orientation := oNoth;
    83{VK_S}    : if GameField.CountSnake > 1 then
                GameField.Snake[2].Orientation := oSouth;
    65{VK_A}    : if GameField.CountSnake > 1 then
                GameField.Snake[2].Orientation := oWest;
    68{VK_D}    : if GameField.CountSnake > 1 then
                GameField.Snake[2].Orientation := oEast;
    end;
  end;
end;

//��������� ����
procedure TForm2.Button5Click(Sender: TObject);
begin
  if not GameField.GameNow then begin            //���� ��� ���� (��������� ���������� ���� ������������ ������ ��� �������� ����)
    //���������� ����� � ����������� ����
    if Form4.ShowModal = mrOk then begin         //���� �������� ���������
      GameField.PlaceMent.X := StrToInt(Form4.Edit1.Text); //�������� ���� - �������������� �������� ����
      GameField.PlaceMent.Y := StrToInt(Form4.Edit2.Text); //�������� ���� - �������������� �������� ����
      GameField.Width := StrToInt(Form4.Edit3.Text);       //�������� ���� - ������ �������� ����
      GameField.Height := StrToInt(Form4.Edit4.Text);      //�������� ���� - ������ �������� ����
      GameField.SizeCellX := StrToInt(Form4.Edit7.Text);   //�������� ���� - ������ ����� ������ �������� ����
      GameField.SizeCellY := StrToInt(Form4.Edit8.Text);   //�������� ���� - ������ ����� ������ �������� ����
    end;
  end;
end;


end.







