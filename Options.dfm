object Form4: TForm4
  Left = 192
  Top = 107
  Width = 410
  Height = 263
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1080#1075#1088#1099
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 216
    Top = 136
    Width = 177
    Height = 41
    Caption = #1055#1088#1080#1085#1103#1090#1100
    ModalResult = 1
    TabOrder = 0
  end
  object Button2: TButton
    Left = 216
    Top = 184
    Width = 177
    Height = 41
    Caption = #1054#1090#1084#1077#1085#1080#1090#1100
    ModalResult = 2
    TabOrder = 1
  end
  object GroupBox2: TGroupBox
    Left = 208
    Top = 16
    Width = 185
    Height = 97
    Caption = #1056#1072#1079#1084#1077#1088#1099' '#1080#1075#1088#1086#1074#1086#1075#1086' '#1087#1086#1083#1103
    TabOrder = 2
    object Label3: TLabel
      Left = 8
      Top = 24
      Width = 16
      Height = 13
      Caption = 'X ='
    end
    object Label4: TLabel
      Left = 8
      Top = 56
      Width = 16
      Height = 13
      Caption = 'Y ='
    end
    object Edit3: TEdit
      Left = 32
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 0
      Text = '500'
      OnKeyPress = Edit1KeyPress
    end
    object Edit4: TEdit
      Left = 32
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 1
      Text = '400'
      OnKeyPress = Edit1KeyPress
    end
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 16
    Width = 185
    Height = 97
    Caption = #1052#1077#1089#1090#1086#1087#1086#1083#1086#1078#1077#1085#1080#1077' '#1080#1075#1088#1086#1074#1086#1075#1086' '#1087#1086#1083#1103
    TabOrder = 3
    object Label1: TLabel
      Left = 8
      Top = 24
      Width = 16
      Height = 13
      Caption = 'X ='
    end
    object Label2: TLabel
      Left = 8
      Top = 56
      Width = 16
      Height = 13
      Caption = 'Y ='
    end
    object Edit1: TEdit
      Left = 32
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 0
      Text = '10'
      OnKeyPress = Edit1KeyPress
    end
    object Edit2: TEdit
      Left = 32
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 1
      Text = '10'
      OnKeyPress = Edit1KeyPress
    end
  end
  object GroupBox4: TGroupBox
    Left = 16
    Top = 128
    Width = 185
    Height = 97
    Caption = #1056#1072#1079#1084#1077#1088#1099' '#1103#1095#1077#1077#1082' '#1080#1075#1088#1086#1074#1086#1075#1086' '#1087#1086#1083#1103
    TabOrder = 4
    object Label7: TLabel
      Left = 8
      Top = 24
      Width = 16
      Height = 13
      Caption = 'X ='
    end
    object Label8: TLabel
      Left = 8
      Top = 56
      Width = 16
      Height = 13
      Caption = 'Y ='
    end
    object Edit7: TEdit
      Left = 32
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 0
      Text = '10'
      OnKeyPress = Edit1KeyPress
    end
    object Edit8: TEdit
      Left = 32
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 1
      Text = '10'
      OnKeyPress = Edit1KeyPress
    end
  end
end
