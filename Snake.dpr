program Snake;

uses
  Forms,
  InitGame in 'InitGame.pas' {Form1},
  Main in 'Main.pas' {Form2},
  Options in 'Options.pas' {Form4},
  SnakeUnit in 'SnakeUnit.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm4, Form4);
  Application.Run;
end.
